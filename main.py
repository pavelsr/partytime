import flask
import json
import requests
import datetime
import os

a = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля', 'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря']

today = datetime.date.today()

# на локалхосте обращаемся к локалхост домену (имя сервиса vk api в docker-compose), на продакшене - к продакшн домену
api_host = 'http://app_vk:5000'
if os.environ.get('VIRTUAL_HOST'):
    api_host = 'https://vk.api.fabmarket.ru'

app = flask.Flask(__name__)

@app.route("/", methods=["GET"])
def main():
    return index.html

@app.route("/api/start", methods=["GET"])
def start_api():
    events = []
    # link = flask.request().form.get()
    answer = requests.get(api_host + '/api/last_posts?days=10&group=http://vk.com/tyumen.university')
    posts = answer.json()['items']
    for post in posts:
        payload = {'text':post.replace('сегодня', str(today.day) + a[today.month%12]).replace('.', '').replace(',', '')}
        answer = requests.post('https://tomita.demo.fabmarket.ru/api/check_text', data=payload).json()
        if answer:
            events.append(answer)
    return flask.Response(json.dumps({'events':events}, ensure_ascii=False), mimetype='application/json')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=4000, debug=True)

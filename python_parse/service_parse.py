from base import *
import json
import requests

app = flask.Flask(__name__)


@app.route("/health", methods = ["GET"])
def asd():
    return "all good"

def parse_group(link, date):

    posts = []

    GROUP_ID = link.replace("http://vk.com/", '').replace("https://vk.com/", '')

    timestamp = time.time() - int(date)*86400
    # day, month, year = (int(i) for i in date.split('.'))
    # COMMENTED FOR BIGGER SERVICE
    # timestamp = datetime.datetime(year, month, day).timestamp()

    i = 0

    while 1:
        temp_posts = api.wall.get(domain = GROUP_ID,
                                count = 100,
                                offset = i*100,
                                v='5.80')['items']
        if temp_posts[-1]['date'] < timestamp:
            for i in range(len(temp_posts)-1, -1, -1):
                if temp_posts[i]['date'] < timestamp:
                    temp_posts.remove(temp_posts[i])
        temp_posts = [i['text'] for i in temp_posts]
        posts.extend(temp_posts)
        if len(temp_posts) < 100:
            break
        # time.sleep(0.3)

    return posts


@app.route('/api/last_posts', methods=["GET"])
def service_parse():
    days = flask.request.args.get('days')
    group = flask.request.args.get('group')
    return flask.Response(json.dumps({'items':parse_group(group, days)}, ensure_ascii=False), mimetype='application/json')

if __name__ == "__main__":
    app.run(host='0.0.0.0')


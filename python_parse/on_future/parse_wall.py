from base import *

posts = []

if len(sys.argv) < 3:
    print(f"Usage: {__name__}.py <group_link> <date>")
    exit()

GROUP_ID = sys.argv[1].replace("http://vk.com/", '').replace("https://vk.com/", '')

day, month, year = (int(i) for i in sys.argv[2].split('.'))

timestamp = datetime.datetime(year, month, day).timestamp()

i = 0

while 1:
    temp_posts = api.wall.get(domain = GROUP_ID,
                              count = 100,
                              offset = i*100,
                              v='5.80')['items']
    if temp_posts[-1]['date'] < timestamp:
        for i in range(len(temp_posts)-1, -1, -1):
            if temp_posts[i]['date'] < timestamp:
                temp_posts.remove(temp_posts[i])
    temp_posts = [i['text'] for i in temp_posts]
    posts.extend(temp_posts)
    if len(temp_posts) < 100:
        break
    time.sleep(0.3)

for post in posts:
    print(post, end='\n\n\n\n')
